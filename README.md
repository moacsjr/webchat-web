Webchat
=====================

A minimal application that combines [MobX](https://mobxjs.github.io/mobx) with [React](https://facebook.github.io/react).
Supports ES6 and JSX compilation through babel.

* Referencia: [mobx-react-todomvc](https://github.com/mobxjs/mobx-react-todomvc).

### Run the example

```
npm install
npm start
open http://localhost:3000
```


### Credits

* [Mendix](http://github.com/mendix) 
* [Mobx](https://mobx.js.org/)