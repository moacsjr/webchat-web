import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
//import DevTools from 'mobx-react-devtools';
import Header from './components/app/Header'
import Auth from './services/Auth.js'
import Message from './components/app/Messages'

@inject('store')
@observer
class App extends Component {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  componentWillMount() {
    if (!Auth.isLoggedIn) {
      this.context.router.push('/login');
    }
  }

  //include <DevTools />
  render() {
    return (
      <div className="container">
        <div>{this.props.children}</div>
      </div>
    );
  }

  onReset = () => {

  }
};

export default App;
