import { observable, computed } from 'mobx';

class AppState{
  @observable message = null;

  @observable selectedContact = null;

  @observable contactList = null;
  @observable friendshipRequestList = null;

  @computed get hasMessage(){
    return !!this.message;
  }

  onInfoMessage(message){
    this.message = {'text':message, 'type':'alert alert-success'}
  }

  onErroMessage(message){
    this.message = {'text':message, 'type':'alert alert-danger'}
  }

}

export default AppState;
