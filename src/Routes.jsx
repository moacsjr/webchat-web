import React, {Component} from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, browserHistory } from 'react-router'
import Sobre from './components/app/Sobre'
import App from './App'
import Dashboard from './components/dashboard/Dashboard'
import Login from './components/app/Login'
import Auth from './services/Auth'

function requireAuth(nextState, replace) {
  if (!Auth.isLoggedIn) {
    replace('/login');
  }
}


class Routes extends Component {
  render() {
    return

  }
}

export default Routes;
