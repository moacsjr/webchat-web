import React, { Component } from 'react';
import { observer } from 'mobx-react';
import UserBar from './UserBar';

var boxStyle = {
  background: '#D8D8D8',
  border: '1px solid #979797',
  width: '100%',
  height: '130px',
  backgroundImage: 'url("/images/WebChatLogo.png")',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'right top',
  marginRight: '200px'
};

var userLogoStyle = {
  width: '100px',
  height: '108px'
}

const Header = ({appState}) =>
<div>
  <UserBar/>
  <div style={boxStyle}>
    <img style={userLogoStyle} src={"/images/FaceChatIcon.png"} />
  </div>
</div>


export default Header;
