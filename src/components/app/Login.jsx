import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
//import Chat from '../../services/Chat'
import Config from '../../services/Config'

@inject('store')
@observer
class Login extends Component {

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  constructor(){
    super();

    //inicializar campos
    this.email = null;
    this.password = null;

    //bind dos metodos
    this.onLoginClick = this.onLoginClick.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.showMessage = this.showMessage.bind(this);
    //Chat.connect();
  }

  render(){
    return (
        <div className="card card-container">
          <img className="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
          <p className="profile-name-card"></p>
                <form className="form-signin">
                    <span className="reauth-email"></span>
                    <input type="email" className="form-control" placeholder="Login" required autoFocus onChange={this.onEmailChange}/>
                    <input type="password" className="form-control" placeholder="Senha" required onChange={this.onPasswordChange}/>
                    <button className="btn btn-lg btn-success btn-block btn-signin" type="button" onClick={this.onLoginClick}>Iniciar Sessão</button>
                </form>
                <Link to="/registro" className="forgot-password">
                    Não é cadastrado? <br/>
                    Clique aqui e registre-se!
                </Link>
        </div>
    )
  }

  onEmailChange(e){
      this.email = e.target.value;
  }

  onPasswordChange(e){
      this.password = e.target.value;
  }

  showMessage(message, className) {
    $.notify(message,  className);
  }

  @action
  onLoginClick(){
    fetch(Config.get('api-endpoint') + '/auth', {
        	method: 'POST',
        	mode: 'cors',
        	redirect: 'follow',
          body: JSON.stringify({username: this.email, password: this.password}),
        	headers: new Headers({
        		'Content-Type': 'application/json'
        	})
        })
    .then((response)=>{
      if(response.ok){
        response.json().then((data)=>{
          Auth.login(this.email, data.token);
          this.props.router.push('/dashboard');
        });
      }
      else{
        this.showMessage('Login inválido. Verifique o usuário e senha.', 'warn');
      }
    })
   .catch((error)=> {
       this.showMessage('Erro interno ao executar a operação.', 'error');
    });
  }


}

export default Login;
