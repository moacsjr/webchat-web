import React, { Component } from 'react';
import {action, observable} from 'mobx'
import { observer, inject } from 'mobx-react';

@inject('store')
@observer
class Message extends Component {

  @observable visible = null

  constructor(){
    super();
    this.visible = true;
  }

  render(){
    if(this.visible){
      setTimeout(()=>{
        this.closeMessage();
      }, 10000);
      return <div className={this.props.store.message.type} role="alert">{this.props.store.message.text}</div>
    }else return null;
  }

  @action
  closeMessage(){
    this.visible = false;
    this.props.store.message = null;
  }
}

export default Message;
