import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Config from '../../services/Config'

@inject('store')
@observer
class Registro extends Component {

  constructor(){
    super();

    this.nome = null;
    this.sobrenome = null;
    this.email = null;
    this.senha = null;

    this.onCadastrarClick = this.onCadastrarClick.bind(this);
    this.onNomeChange = this.onNomeChange.bind(this);
    this.onSobrenomeChange = this.onSobrenomeChange.bind(this);
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onSenhaChange = this.onSenhaChange.bind(this);
    this.isValido = this.isValido.bind(this);
    this.showMessage = this.showMessage.bind(this);
  }

  static contextTypes = {
    router: React.PropTypes.object.isRequired
  };

  render(){
    return (
      <div className="card card-container">
        <img className="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
        <p className="profile-name-card"></p>
          <form className="form-signin">
              <span className="reauth-email"></span>
              <input type="text"  className="form-control"  placeholder="Nome" required autoFocus onChange={this.onNomeChange}/>
              <input type="text"  className="form-control"  placeholder="Sobrenome" required autoFocus onChange={this.onSobrenomeChange}/>
              <input type="email" className="form-control"  placeholder="Login" required autoFocus onChange={this.onEmailChange}/>
              <input type="password" className="form-control" placeholder="Senha" required onChange={this.onSenhaChange}/>
              <button className="btn btn-lg btn-success btn-block btn-signin" type="button" onClick={this.onCadastrarClick}>Cadastrar</button>
          </form>
      </div>
    );
  }

  onNomeChange(e){
    this.nome = e.target.value;
  }

  onSobrenomeChange(e){
    this.sobrenome = e.target.value;
  }

  onSenhaChange(e){
    this.senha = e.target.value;
  }

  onEmailChange(e){
    this.email = e.target.value;
  }

  isValido(){
    return this.nome && this.sobrenome && this.email && this.senha ;
  }

  showMessage(message, className) {
    $.notify(message,  className);
  }

  @action
  onCadastrarClick() {
    if(this.isValido()){
      fetch(Config.get('api-endpoint') +'/users', {
          	method: 'POST',
          	mode: 'cors',
          	redirect: 'follow',
            body: JSON.stringify({firstname: this.nome, lastname: this.sobrenome, email: this.email, password: this.senha }),
          	headers: new Headers({
          		'Content-Type': 'application/json'
          	})
      })
      .then((response)=>{
        if(response.ok){
          this.props.router.push('/login');
        }else{
          return response.json();
        }
      })
      .then((erroMsg)=>{
        this.showMessage(erroMsg.error+ ' - ' + erroMsg.message, 'warn');
      })
      .catch(()=>{
        this.showMessage('Ocorreu um erro ao realizar a operação', 'error');
      });
    }else{
      $.notify('Favor informar todos os campos.', 'warn');
    }
  }

}

export default Registro;
