import React, { Component } from 'react';
import { observer } from 'mobx-react';
import {observable, action} from 'mobx';

var boxStyle = {
  background: 'black',
  border: '1px solid black',
  width: '100%',
  height: '20px'
};

var link = {
  color: 'white'
}

@observer
class UserBar extends Component {

  @observable showLoginPage = false;

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  render(){
    if(this.showLoginPage===false)
      return <div style={boxStyle}> <a style={link} onClick={this.handleClick}>Click to Login</a></div>
    else return <LoginForm />
  }

  @action
  handleClick(){
    this.showLoginPage = !this.showLoginPage;
  }
}

export default UserBar;
