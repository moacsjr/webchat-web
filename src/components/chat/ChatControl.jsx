import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Config from '../../services/Config'
import ContactRepository from '../../repository/ContactRepository.js'
import Chat from '../../services/Chat'


@inject('store')
@observer
class ChatControl extends Component {

  constructor(){
    super();
    this.setTextMessage = this.setTextMessage.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.textMessage= null;
    this.textInput =null;
  }

  @action
  sendMessage(){
    Chat.sendMessage(this.props.store.selectedContact.friendId, this.textMessage);
    this.textInput.value = '';
  }

  setTextMessage(e){
    this.textMessage = e.target.value;
  }

  render(){
    if (!this.props.store.selectedContact){
      return null;
    }
    return (
      <div className="input-group">
        <input type="text" className="form-control"
        placeholder="Escreva uma mensagem ..." onChange={this.setTextMessage}
        ref={(input) => { this.textInput = input; }} />
        <span className="input-group-btn">
          <button className="btn btn-default" type="button" onClick={this.sendMessage}>Go!</button>
        </span>
      </div>
    )
  }
}

export default ChatControl;
