import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Chat from '../../services/Chat'

@inject('store')
@observer
class ChatPanel extends Component {

  constructor(){
    super();

  }

  render(){
    let fromMe = this.props.from === Auth.user;
    let align = (fromMe)? 'left' : 'right';
    let ico = (fromMe)? 'ME' : 'U';
    let timeWidget = <small className={((fromMe)?'pull-right' : '') + ' text-muted'}>  <span className="glyphicon glyphicon-time"></span>12 mins ago</small>;
    return (
        <li className={align + ' clearfix'}><span className={'chat-img pull-' + align}>
            <img src={'http://placehold.it/50/55C1E7/fff&text=' + ico} alt="User Avatar" className="img-circle" />
        </span>
            <div className="chat-body clearfix">
                <div className="header">
                    {(!fromMe)? timeWidget : null}
                    <strong className={(!fromMe)? 'primary-font pull-right': 'primary-font'}>{this.props.from}</strong>
                    {(fromMe)? timeWidget : null}
                </div>
                <p>
                    {this.props.message}
                </p>
            </div>
        </li>

    )
  }
}

export default ChatPanel;
