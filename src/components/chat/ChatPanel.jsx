import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Config from '../../services/Config'
import ContactRepository from '../../repository/ContactRepository.js'
import Chat from '../../services/Chat'
import ChatMessage from '../chat/ChatMessage'


@inject('store')
@observer
class ChatPanel extends Component {

  constructor(){
    super();

  }

  render(){
    if (!this.props.store.selectedContact){
      return null;
    }

    let messages = Chat.chatMessages.get(this.props.store.selectedContact.friendId);
    let content = null;
    if(messages){
      content = messages.map((item)=>
        <ChatMessage key={item.uid} message={item.message} from={item.from} />
      );
    }

    return (
      <div className="panel panel-primary">
        <div className="panel-heading">Sala de Bate Papo com {this.props.store.selectedContact.friendId}</div>
        <div className="panel-body">
          <ul className="chat">
            {content}
          </ul>
        </div>
      </div>
    )
  }
}

export default ChatPanel;
