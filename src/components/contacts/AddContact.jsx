import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import ContactRepository from '../../repository/ContactRepository.js'

@inject('store')
@observer
class AddContact extends Component {

  constructor(){
    super()
    this.setTextMessage = this.setTextMessage.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.setContactList = this.setContactList.bind(this);
    this.textInput =null;
  }

  @action
  sendMessage(){
    ContactRepository.sendFriendshipRequest(this.textMessage).then(()=>{
        ContactRepository.fetchContacts().then((contacts)=>{
          this.setContactList(contacts);
        });
    });
    this.textInput.value = '';
  }

  @action
  setContactList(contacts){
    this.props.store.contactList = contacts;
  }

  setTextMessage(e){
    this.textMessage = e.target.value;
  }

  render(){
    return (
    <div className="input-group">
      <input type="text" className="form-control"
      placeholder="Informe o email para adicionar" onChange={this.setTextMessage}
      ref={(input) => { this.textInput = input; }} />
      <span className="input-group-btn">
        <button className="btn btn-default" type="button" onClick={this.sendMessage}>Solicitar Amizade</button>
      </span>
    </div>
  )
  }
}

export default AddContact;
