import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action, observable} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Config from '../../services/Config'
import Chat from '../../services/Chat'
import ContactRepository from '../../repository/ContactRepository.js'

@inject('store')
@observer
class Contact extends Component{

  constructor(){
    super();
    this.onSelect = this.onSelect.bind(this);
    this.onAccept = this.onAccept.bind(this);
    this.onBlock = this.onBlock.bind(this);
  }

  @action
  onAccept(){
    ContactRepository.acceptRequest(this.props.name).then(action((resp)=>{
      this.props.contact.status = 'ACEITO';
      Chat.sendContactUpdates(this.props.name);
    }));
  }

  @action
  onBlock(){
    ContactRepository.blockRequest(this.props.name).then(action((resp)=>{
      this.props.contact.status = 'BLOQUEANDO';
      Chat.sendContactUpdates(this.props.name);
    }));
  }

  @action
  onSelect(){
    this.props.store.selectedContact = this.props.contact;
  }

  render(){

    let aceitarAction = ((this.props.contact.status === 'PENDENTE') && this.props.contact.friendId === Auth.user) ? <button type="button" className="btn btn-info" onClick={this.onAccept}>Aceitar</button> : null;
    let desbloquearAction = (this.props.contact.status === 'BLOQUEANDO') ? <button type="button" className="btn btn-info" onClick={this.onAccept}>Aceitar</button> : null;
    let lblBloqueado = (this.props.contact.status === 'BLOQUEADO' || this.props.contact.status === 'BLOQUEANDO') ? <span className="label label-danger">Bloqueado</span> : null;
    let lblPendente = (this.props.contact.status === 'PENDENTE' && this.props.contact.username === Auth.user) ? <span className="label label-warning">Pendente</span> : null;
    let lblAceito = (this.props.contact.status === 'ACEITO') ? <span className="label label-success">Aceito</span> : null;
    let bloquearAction = (!lblPendente && (this.props.contact.status !== 'BLOQUEANDO' && this.props.contact.status !== 'BLOQUEADO')) ? <button type="button" className="btn btn-danger" onClick={this.onBlock}>Bloquear</button> : null;

    return (
      <a href="#" className={'list-group-item '+ (this.props.selected?'active':'')} onClick={this.props.onSelect}>
        {this.props.name} {lblBloqueado} {lblPendente} {lblAceito}
        <div className="contact-actions">
          {aceitarAction} {bloquearAction} {desbloquearAction}
        </div>
      </a>
    )
  }
}

export default Contact;
