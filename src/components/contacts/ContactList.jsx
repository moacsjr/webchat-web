import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Config from '../../services/Config'
import ContactRepository from '../../repository/ContactRepository.js'
import Contact from './Contact'

@inject('store')
@observer
class ContactList extends Component {

  constructor(){
    super();
    this.setContactList=this.setContactList.bind(this);
    ContactRepository.fetchContacts().then((contacts)=>{
      this.setContactList(contacts);
    });
  }

  @action
  setContactList(contacts){
    this.props.store.contactList = contacts;
  }

  render(){
    if (!this.props.store.contactList){
      return null;
    }
    let contacts = this.props.store.contactList.map((contact)=>
      <Contact  key={contact.friendId} contact={contact}
                name={contact.friendId}
                selected={(this.props.store.selectedContact === contact)}
                onSelect={action(()=>this.props.store.selectedContact = contact)}/>
    );
    return (
      <div className="list-group">
        {contacts}
      </div>
    )
  }
}

export default ContactList;
