import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Config from '../../services/Config'
import ContactRepository from '../../repository/ContactRepository.js'
import Contact from './Contact'

@inject('store')
@observer
class FriendshipRequestList extends Component {

  constructor(){
    super();
    this.setRequestList=this.setRequestList.bind(this);
    ContactRepository.fetchFriendshipRequest().then((requests)=>{
      this.setRequestList(requests);
    });
  }

  @action
  setRequestList(requests){
    this.props.store.friendshipRequestList = requests;
  }

  render(){
    if (!this.props.store.friendshipRequestList){
      return null;
    }
    let requests = this.props.store.friendshipRequestList.map((request)=>
      <Contact key={request.username} contact={request} name={request.username} selected={false}/>
    );
    return (
      <div className="list-group">
        {requests}
      </div>
    )
  }
}

export default FriendshipRequestList;
