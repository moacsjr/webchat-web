import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Config from '../../services/Config'

@inject('store')
@observer
class AppBar extends Component {

  constructor(){
    super()
    this.logoff = this.logoff.bind(this);
  }

  componentDidMount(){
    let user = Auth.getUserInfo()
  }

  logoff(){
    Auth.logoff();
  }

  render(){
    let user = Auth.getUserInfo();
    if(!user){
      return null;
    }
    return (
      <div className='row app-bar'>
        <div className='col-md-4'>
          <span>Conectado como {user.firstname} {user.lastname}</span>
        </div>
        <div className='col-md-4'>
            <div><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Selecione um usuário para iniciar o chat</div>
        </div>
        <div className='col-md-4'>
          <div className='pull-right'>
            <a className='' onClick={this.logoff}>
              <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
              Logoff
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default AppBar;
