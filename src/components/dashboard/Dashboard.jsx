import React, { Component } from 'react';
import { Router, Route, Link } from 'react-router'
import {action} from 'mobx'
import { observer, inject } from 'mobx-react';
import Auth from '../../services/Auth'
import Chat from '../../services/Chat'
import Config from '../../services/Config'
import AppBar from './AppBar'
import ContactList from '../contacts/ContactList'
import AddContact from '../contacts/AddContact'
import ChatPanel from '../chat/ChatPanel'
import ChatControl from '../chat/ChatControl'
import FriendshipRequestList from '../contacts/FriendshipRequestList'

@inject('store')
class Dashboard extends Component {

  componentDidMount(){
    Chat.connect();
    Chat.store = this.props.store;
  }

  render(){
    return (
      <div className="dashboard">
        <AppBar />
        <div className="row">
          <div className="col-md-4">
            <AddContact />
            <FriendshipRequestList />
            <ContactList />
          </div>
          <div className="col-md-8">
            <ChatPanel />
            <ChatControl />
          </div>
        </div>
      </div>
    )
  }
}


export default Dashboard;
