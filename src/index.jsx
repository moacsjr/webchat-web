import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import Routes from './Routes';
import { Router, Route, Link, browserHistory } from 'react-router'
import { Provider } from 'mobx-react'
import AppState from './AppState'
import App from './App'
import Sobre from './components/app/Sobre'
import Dashboard from './components/dashboard/Dashboard'
import Login from './components/app/Login'
import Registro from './components/app/Registro'
import Auth from './services/Auth'
import cookie from 'react-cookie';

const appState = new AppState();

function requireAuth(nextState, replace) {
  let user = {
    userId: cookie.load('userId'),
    token: cookie.load('token')
   };
   if(user.token && user.userId){
     Auth.login(user.userId, user.token);
   }
  if (!Auth.isLoggedIn) {
    replace('/login');
  }
}

render(
  <Provider store={appState} >
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <Route path="sobre" component={Sobre}/>
      <Route name="login" path="login" component={Login} />
      <Route path="registro" component={Registro}/>
      <Route path="dashboard" onEnter={requireAuth} component={Dashboard}/>
    </Route>
  </Router>
  </Provider>
  ,
  document.getElementById('root')
);
