import { observable, computed } from 'mobx';
import jwt from 'jsonwebtoken';
import singleton from 'singleton';
import Auth from '../services/Auth';
import Config from '../services/Config'
import { observer, inject } from 'mobx-react';

class ContactRepository extends singleton {

  constructor(){
    super()
    this.showMessage = this.showMessage.bind(this);
  }

  showMessage(message, className) {
    $.notify(message,  className);
  }

  fetchFriendshipRequest(){
    return new Promise(function(resolve, reject){
      let uri = '/friendship/' + Auth.user + '/pending';
      fetch(Config.get('api-endpoint') + uri, {
          	method: 'GET',
          	mode: 'cors',
          	redirect: 'follow',
            //body: JSON.stringify({username: this.email, password: this.password}),
          	headers: new Headers({
          		'Content-Type': 'application/json',
              'Authorization': 'Bearer '+Auth.token
          	})
      }) //fetch
      .then((response)=>{
        if(response.ok){
          resolve(response.json());
        }else{
          reject(response.error());
        }
      })
    });
  }//fetchFriendshipRequest

  fetchContacts(){
    return new Promise(function(resolve, reject){
      let uri = '/friendship/' + Auth.user + '/';
      fetch(Config.get('api-endpoint')+ uri, {
          	method: 'GET',
          	mode: 'cors',
          	redirect: 'follow',
            //body: JSON.stringify({username: this.email, password: this.password}),
          	headers: new Headers({
          		'Content-Type': 'application/json',
              'Authorization': 'Bearer '+Auth.token
          	})
      }) //fetch
      .then((response)=>{
        if(response.ok){
          resolve(response.json());
        }else{
          reject(response.error());
        }
      })
    });
  }//fetchContacts

  acceptRequest(friend){
    return new Promise(function(resolve, reject){
      let uri = '/friendship/' + Auth.user + '/accept/' + friend + '/';
      fetch(Config.get('api-endpoint')+ uri, {
          	method: 'POST',
          	mode: 'cors',
          	redirect: 'follow',
            //body: JSON.stringify({username: this.email, password: this.password}),
          	headers: new Headers({
          		'Content-Type': 'application/json',
              'Authorization': 'Bearer '+Auth.token
          	})
      }) //fetch
      .then((response)=>{
        if(response.ok){
          resolve('ok');
        }else{
          reject(response.error());
        }
      })
    });
  }//acceptRequest

  blockRequest(friend){
    return new Promise(function(resolve, reject){
      let uri = '/friendship/' + Auth.user + '/block/' + friend + '/';
      fetch(Config.get('api-endpoint')+ uri, {
          	method: 'POST',
          	mode: 'cors',
          	redirect: 'follow',
            //body: JSON.stringify({username: this.email, password: this.password}),
          	headers: new Headers({
          		'Content-Type': 'application/json',
              'Authorization': 'Bearer '+Auth.token
          	})
      }) //fetch
      .then((response)=>{
        if(response.ok){
          resolve('ok');
        }else{
          reject(response.error());
        }
      })
    });
  }//acceptRequest

  unblockRequest(friend){
    return new Promise(function(resolve, reject){
      let uri = '/friendship/' + Auth.user + '/unblock/' + friend + '/';
      fetch(Config.get('api-endpoint') + uri , {
          	method: 'POST',
          	mode: 'cors',
          	redirect: 'follow',
            //body: JSON.stringify({username: this.email, password: this.password}),
          	headers: new Headers({
          		'Content-Type': 'application/json',
              'Authorization': 'Bearer '+Auth.token
          	})
      }) //fetch
      .then((response)=>{
        if(response.ok){
          resolve('ok');
        }else{
          reject(response.error());
        }
      })
    });
  }//acceptRequest

  sendFriendshipRequest(email){
    return new Promise(function(resolve, reject){
      let uri = '/friendship/' + Auth.user + '/request';
      fetch(Config.get('api-endpoint') + uri , {
          	method: 'POST',
          	mode: 'cors',
          	redirect: 'follow',
            body: JSON.stringify({username: Auth.user, friendId: email, friendNickName: email}),
          	headers: new Headers({
          		'Content-Type': 'application/json',
              'Authorization': 'Bearer '+Auth.token
          	})
      }) //fetch
      .then((response)=>{
        if(response.ok){
          resolve('ok');
        }else{
          reject(response.error());
        }
      })
    });
  }//acceptRequest



}

export default ContactRepository.get();
