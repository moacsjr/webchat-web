
import { observable, computed } from 'mobx';
import jwt from 'jsonwebtoken';
import singleton from 'singleton';
import cookie from 'react-cookie';
import Config from './Config'

var JWT_SECRET = 'mySecret';

class Auth extends singleton {
  @observable user = null;
  @observable userDetail = null;

  @computed get isLoggedIn() {
    return !!this.user;
  }

  constructor() {
    super();
    this.getUserDetail = this.loadUserDetail.bind(this);
    this.getToken = this.getToken.bind(this);
    this.getUserInfo = this.getUserInfo.bind(this);

    this.token=false;
    this.userDetail = {username: null, lastname: null, email:  null};

    if (this.token) {
      this.user = jwt.verify(token, JWT_SECRET);
    }
  }

  login(username, token) {
    this.token = token;
    this.user=username;
    cookie.save('userId', username, { path: '/' });
    cookie.save('token', token, { path: '/' });
    this.loadUserDetail();
  }

  logout() {
    this.token = false;
    this.username = null;
    cookie.remove('userId', { path: '/' });
    cookie.remove('token', { path: '/' });
  }

  getUserInfo(){
    return this.userDetail;
  }

  getToken(){
    return this.token;
  }

  loadUserDetail(){
    fetch(Config.get('api-endpoint') + '/users/' + this.user, {
        	method: 'GET',
        	mode: 'cors',
        	redirect: 'follow',
        	headers: new Headers({
        		'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this.getToken()
        	})
    })
    .then((resp)=>{
      if(resp.ok){
        return resp.json();
      }else{
        throw 'Erro ao carregar detalhes do usuário';
      }
    })
    .then((userData)=>{
      this.userDetail = userData;
    })
  }
}

export default Auth.get();
