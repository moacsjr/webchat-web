import { observable, computed, action } from 'mobx'
import jwt from 'jsonwebtoken'
import singleton from 'singleton'
import Auth from './Auth'
import ContactRepository from '../repository/ContactRepository.js'

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

class Chat extends singleton {

  @observable chatMessages = null;

  constructor(){
    super();
    this.chatMessages = new Map();
    this.socket = null;//new SockJS('http://localhost:8080/webchat-websocket?token='+Auth.getToken());
    this.stompClient = null;
    this.connected = false;
    this.connect = this.connect.bind(this);
    this.setConnected = this.setConnected.bind(this);
    this.addMessage = this.addMessage.bind(this);
    this.showMessage = this.showMessage.bind(this);
    this.store = null
  }

  showMessage(message, className) {
    $.notify(message,  className);
  }

  connect() {
    if(this.connected)return;
    this.socket = new SockJS('http://webchatapi:8080/webchat-websocket?token='+Auth.getToken());
    this.stompClient = Stomp.over(this.socket);
    let headers = {'X-Authorization':'TOKEN '+Auth.getToken()};
    this.stompClient.connect(headers, (frame) => {
        this.setConnected(true);
        console.log('Connected: ' + frame);
        this.sendLoginUpdates();
        this.stompClient.subscribe('/user/queue/chat-updates', action( (message) => {
            console.log('Message Received = ' + message);
            if (message.body) {
                let msgObj = JSON.parse(message.body);
                this.addMessage(msgObj);
            }else{
              console.log('Empty Message Received from websocket');
            }

        }));//
        this.stompClient.subscribe('/user/queue/contact-updates', action( (message) => {
            console.log('Message Received = ' + message);
            ContactRepository.fetchContacts().then(action((contacts)=>{
              this.store.contactList = contacts;
            }));

        }));//
        this.stompClient.subscribe('/queue/login-updates', action( (message) => {
            console.log('Message Received = ' + message);
            let msgObj = JSON.parse(message.body)
            if (msgObj.from === Auth.user) return;
            this.showMessage(`O usuario ${msgObj.from} está online`, 'info')
        }));//
    });
  }

  @action
  addMessage(msgObj){
    let from = (msgObj.from === Auth.user)? msgObj.to : msgObj.from;
    let userMessages = this.chatMessages.get(from);
    if(!userMessages){
      userMessages = observable([]);
    }
    userMessages.push(msgObj);
    this.chatMessages.set(from, userMessages);
  }

  sendMessage(to, message){
    if(this.connected){
      let msgObj = {'to': to, 'message': message};
      this.stompClient.send('/app/chat', {}, JSON.stringify(msgObj));
    }else{
      console.log('not connected');
    }
  }

  sendContactUpdates(to){
    if(this.connected){
      let msgObj = {'to': to, 'message': 'contactupdate'};
      this.stompClient.send('/app/contactupdate', {}, JSON.stringify(msgObj));
    }else{
      console.log('not connected');
    }
  }

  sendLoginUpdates(){
    if(this.connected){
      let msgObj = {'to': 'TODOS', 'from': Auth.user, 'message': 'login_update'};
      this.stompClient.send('/app/login', {}, JSON.stringify(msgObj));
    }else{
      console.log('not connected');
    }
  }

  setConnected(status){
    this.connected = status;
  }

}

export default Chat.get();
