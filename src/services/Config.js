import { observable, computed } from 'mobx';
import jwt from 'jsonwebtoken';
import singleton from 'singleton';

class Config extends singleton {

  constructor(){
    super();
    this.get = this.get.bind(this);
    this.params = new Map();
    this.params.set('websocket-endpoint', 'http://webchatapi:8080');
    this.params.set('api-endpoint', 'http://webchatapi:8080');
  }

  get(key){
    return this.params.get(key);
  }

}

export default Config.get();
